package pregunta2;
import java.util.*;
/**
 * @author Melina Karen Ticra Aguilar 
 * CI:  15208024
 */
public class Pregunta2 {
    public static void main(String[] args) {
        Lista<Integer> list = new Lista<>();
        Scanner sc = new Scanner(System. in);
        
        int op;
        int yes = 0;
        Lista l_use = new Lista<>();
        System.out.println("Menu para Lista");
        System.out.println("Que desea hacer?");
        
        while(yes == 0 && l_use != null){
        System.out.println("1.- verificar si está vacía");
        System.out.println("2.- tamaño de la lista");
        System.out.println("3.- recuperar un valor especifico");
        System.out.println("4.- recuperar el primer valor");
        System.out.println("5.- recuperar el ultimo valor");
        System.out.println("6.- añadir al último");
        System.out.println("7.- añadir al principio");
        System.out.println("8.- añadir donde queramos");
        System.out.println("9.- eliminar el primero");
        System.out.println("10.- eliminar el ultimo");
        System.out.println("11.- eliminar cualquiera");
        System.out.println("12.- modificar un valor de la lista");
        System.out.println("13.- ver el índex de un nodo");
        op = sc.nextInt();
        switch(op){
                case 1:
                    if(list.Vacia()==true)
                    System.out.println(list.Vacia());
                    else
                        System.out.println("lista vacia");
                    break;
                case 2:
                    System.out.println("el tamaño es");
                    System.out.println(list.tamList());
                    break;
                case 3:
                    
                    System.out.println(list.get(yes));
                    break;
                case 4:
                    System.out.println("El primer valor es");
                    System.out.println(list.getFirst());
                    break;
                case 5:
                    System.out.println("el segundo valor es");
                    System.out.println(list.getLast());
                    break;
                case 6:
                    System.out.println(list.alfinal(yes));
                    break;
                case 7:
                    System.out.println(list.AlPrincipio(yes));
                    break;
                case 8:
                    System.out.println(list.añadir(yes, yes));
                    break;
                case 9:
                    System.out.println(list.removeFirst());
                    break;
                case 10:
                    System.out.println(list.removeLast());
                    break;
                case 11:
                    System.out.println(list.remove(yes));
                    break;
                case 12:
                    System.out.println(list.modificar(yes, yes));
                    break;
                case 13:
                    System.out.println(list.index(yes));
                    break;
                default:
                    break;
                

        }
        
    }
    
    }
}
