package principal;
/**
 *@author Melina Karen Ticra Aguilar 
 * CI:  15208024
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Proceso<Integer> list = new Proceso<>();
        System.out.println(list.Vacia());
        System.out.println("add elemento al ultimo "+list.alfinal(1));
        System.out.println("add elemento al ultimo "+list.alfinal(2));
        System.out.println("add elemento al ultimo "+list.alfinal(3));
        System.out.println("add element  al principio"+list.AlPrincipio(4));
        System.out.println("add elemento al ultimo "+list.alfinal(5));
        System.out.println("add elemento al ultimo "+list.alfinal(6));
        System.out.println("add element  al a la posicion 1"+list.add(7,3));
        System.out.println("add element  al a la posicion 1"+list.add(8,2));
        System.out.println(list.listContent());
        System.out.println("eliminado el primero "+list.removeFirst());
        System.out.println("eliminado el ultimo "+list.removeLast());
        System.out.println(list.listContent());
        System.out.println("eliminado del index 2 "+list.remove(3));
        System.out.println(list.listContent());
        System.out.println("el index del numero 8 es :"+ list.indexOf(8));      
        System.out.println("el index del numero 10 es :"+ list.indexOf(10));  
        System.out.println("modificando el elemento del index  2"+ list.modify(66,2));
        System.out.println(list.listContent());

    }
    
}
