package principal;
/**
 * @author Melina Karen Ticra Aguilar 
 * CI:  15208024
 */
public class Nodo1<G> {
    private G elem;
    private Nodo1<G> Sig;
    
    public Nodo1(G elem, Nodo1<G> Sig){
        this.elem = elem;
        this.Sig = Sig;
    }

    public G getElement() {
        return elem;
    }

    public void setElement(G elemento) {
        this.elem = elemento;
    }

    public Nodo1<G> getNext() {
        return Sig;
    }

    public void setNext(Nodo1<G> Sig) {
        this.Sig = Sig;
    }
}
