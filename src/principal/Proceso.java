package principal;
/** @author Melina Karen Ticra Aguilar
 */
public class Proceso<G> {
    private Nodo1<G> primero;
    private Nodo1<G> Ultimo;
    private int size;

    public Proceso() {
        this.primero = null;
        this.Ultimo = null;
        this.size = 0;
    }

    public boolean Vacia() {
        return size == 0;
    }

    public int sizeList() {
        return size;
    }

    private Nodo1<G> ObNodo(int lugar) {
        if (Vacia() || (lugar < 0 || lugar >= sizeList())) {
            return null;
        } else if (lugar == 0) {
            return primero;
        } else if (lugar == sizeList() - 1) {
            return Ultimo;
        } else {
            Nodo1<G> search = primero;
            int count = 0;
            while (count < lugar) {
                count++;
                search = search.getNext();
            }
            return search;
        }
    }

    public G get(int lugar) {
        if (Vacia() || (lugar < 0 || lugar >= sizeList())) {
            return null;
        } else if (lugar == 0) {
            return primero.getElement();
        } else if (lugar == sizeList() - 1) {
            return Ultimo.getElement();
        } else {
            Nodo1<G> search = ObNodo(lugar);
            return search.getElement();
        }
    }

    public G getFirst() {
        if (Vacia()) {
            return null;
        } else {
            return primero.getElement();
        }
    }

    public G getLast() {
        if (Vacia()) {
            return null;
        } else {
            return Ultimo.getElement();
        }
    }

    public G AlPrincipio(G element) {
        Nodo1<G> nuevelement;
        if (Vacia()) {
            nuevelement = new Nodo1<>(element, null);
            primero = nuevelement;
            Ultimo = nuevelement;
        } else {
            nuevelement = new Nodo1<G>(element,primero);
            primero = nuevelement;
        }
        size++;
        return primero.getElement();
    }

    public G alfinal(G element) {
        Nodo1<G> newelement;
        if (Vacia()) {
            return AlPrincipio(element);
        } else {
            newelement = new Nodo1<G>(element,null);
            Ultimo.setNext(newelement);
            Ultimo = newelement;
        }
        size++;
        return Ultimo.getElement();
    }

    public G add(G element, int index) {
        if (index == 0) {
            return AlPrincipio(element);
        } else if(index == sizeList()){
            return alfinal(element);
        }else if((index<0 || index >= sizeList())){
            return null;
        }else{
            Nodo1<G> nodo_prev = ObNodo(index - 1);
            Nodo1<G> nodo_current = ObNodo(index);
            Nodo1<G> newelement = new Nodo1<>(element,nodo_current);
            nodo_prev.setNext(newelement);
            size++;
            return ObNodo(index).getElement();
        }
    }
    
    public String listContent(){
        String str = "";
        if(Vacia()){
            str = "LISTA VACIA";
        }else{
           Nodo1<G> out = primero;
           while(out != null){
               str += out.getElement()+" - ";
               out = out.getNext();
           }
        }
        return str;
    }
    
    public G removeFirst(){
        if(Vacia()){
            return null;
        }else{
            G element = primero.getElement();
            Nodo1<G> aux = primero.getNext();
            primero = aux;
            if(sizeList() == 1){
                Ultimo = null;
            }
            size--;
            return element;
        }
    }
    
    public G removeLast(){
        if(Vacia()){
            return null;
        }else{
            G element = Ultimo.getElement();
            Nodo1<G> newLast = ObNodo(sizeList()-2);
            if(newLast == null){
                Ultimo = null;
                if(sizeList() == 2){
                    Ultimo = primero;
                }else{
                    primero = null;
                }
            }else{
                Ultimo = newLast;
                Ultimo.setNext(null);
            }
            size--;
            return element;
        }
    }
    
    public G remove(int index){
        if (index == 0) {
            return removeFirst();
        } else if(index == sizeList()){
            return removeLast();
        }else if(Vacia() || (index<0 || index >= sizeList())){
            return null;
        }else{
            Nodo1<G> nodo_prev = ObNodo(index-1);
            Nodo1<G> nodo_current = ObNodo(index);
            Nodo1<G> nodo_current_next = nodo_current.getNext();
            G element = nodo_current.getElement();
            nodo_current = null;
            nodo_prev.setNext(nodo_current_next);
            size--;
            return element;
        }
    }
    public int indexOf(G element){
        if(Vacia()){
            return -1;
        }else{
            Nodo1<G> aux = primero; 
            int index = 0;
            while(aux != null){
                if(element == aux.getElement()){
                    return index;
                }
                index++;
                aux = aux.getNext();
            }
        }
        return -1;
    }
    public G modify(G newValue, int index){
        if(Vacia() || (index<0 || index >= sizeList())){
            return null;
        }else{
            Nodo1<G> aux = ObNodo(index);
            aux.setElement(newValue);
            return aux.getElement();
        }
    }    

}
